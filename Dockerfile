FROM openjdk:17-jdk-alpine
MAINTAINER SS
COPY target/SistemaProductos-0.0.1-SNAPSHOT.jar ss-app.jar
ENTRYPOINT ["java", "-jar", "/ss-app.jar"]