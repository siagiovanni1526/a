package com.sistemaProductos.SistemaProductos.service;

import com.sistemaProductos.SistemaProductos.model.Usuario;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class UsuarioService implements IUsuarioService {
  @Override
  public List<Usuario> getUsuarios() {
    return null;
  }

  @Override
  public void eliminar(Long id) {

  }

  @Override
  public void registrar(Usuario usuario) {

  }

  @Override
  public Usuario obtenerUsuarioPorCredenciales(Usuario usuario) {
    return null;
  }
}
