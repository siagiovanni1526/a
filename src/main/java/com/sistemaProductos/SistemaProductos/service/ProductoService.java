package com.sistemaProductos.SistemaProductos.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.internal.FirebaseRequestInitializer;
import com.sistemaProductos.SistemaProductos.config.FirebaseConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistemaProductos.SistemaProductos.model.Producto;
import com.sistemaProductos.SistemaProductos.repository.IProductoRepository;

@Service
public class ProductoService implements IProductoService{

	@Autowired
	private FirebaseConfig firebase;

	@Override
	public Boolean create(Producto producto) {
		Map<String, Object> docData = new HashMap<>();
		docData.put("descripcion",producto.getDescripcion());
		docData.put("genero",producto.getGenero());
		docData.put("id_tipo_producto",producto.getTipoProducto().getId());
		docData.put("imagen",producto.getImagen());
		docData.put("nombre",producto.getNombre());
		docData.put("precio",producto.getPrecio());
		CollectionReference posts = firebase.getFirestore().collection("post");
		ApiFuture<WriteResult> writeResultApiFuture = posts.document().create(docData);

		try {
			if(null != writeResultApiFuture.get()){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}catch (Exception e) {
			return Boolean.FALSE;
		}
		//return this.productoRepo.save(producto);
	}

	@Override
	public Producto update(Producto producto) {
		return null;
	}

	@Override
	public Producto findById(Integer id) {
		return null;
	}

	@Override
	public List<Producto> findAll() {
		return null;
	}

	@Override
	public void delete(Integer id) {

	}


}
