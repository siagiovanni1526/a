package com.sistemaProductos.SistemaProductos.service;

import com.sistemaProductos.SistemaProductos.model.TipoProducto;
import com.sistemaProductos.SistemaProductos.repository.ITipoProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TipoProductoService implements ITipoProductoService {

  @Override
  public TipoProducto create(TipoProducto tipoProducto) {
    return null;
  }

  @Override
  public TipoProducto update(TipoProducto tipoProducto) {
    return null;
  }

  @Override
  public TipoProducto findById(Long id) {
    return null;
  }

  @Override
  public List<TipoProducto> findAll() {
    return null;
  }

  @Override
  public void deleteTipoProd(Long id) {

  }
}
