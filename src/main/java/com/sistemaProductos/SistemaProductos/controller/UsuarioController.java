package com.sistemaProductos.SistemaProductos.controller;

import java.util.List;

import com.sistemaProductos.SistemaProductos.model.Usuario;
import com.sistemaProductos.SistemaProductos.service.UsuarioService;
import com.sistemaProductos.SistemaProductos.utils.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

// Los controllers sirven para manejar las direcciones de URL

// indicamos que es una clase controlador
@RestController
public class UsuarioController {

}